using namespace std;

class No{

	private:
		int info;
		No *next;
		No *back;

	public:
		void setInfo(int info){
			this->info = info;
		}
		int getInfo(){
			return info;
		}
		void setNext(No *next){
			this->next = next;
		}
		No *getNext(){
			return next;
		}
		void setBack(No *back){
			this->back = back;
		}
		No *getBack(){
			return back;
		}
};
